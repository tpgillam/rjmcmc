(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     13899,        401]
NotebookOptionsPosition[     13016,        364]
NotebookOutlinePosition[     13371,        380]
CellTagsIndexPosition[     13328,        377]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Bezier curve formulation", "Section",
 CellChangeTimes->{{3.631916052580193*^9, 3.631916057498274*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"B1", "[", 
    RowBox[{"t_", ",", "p0_", ",", "p1_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{"p0", "+", 
     RowBox[{"t", "*", 
      RowBox[{"(", 
       RowBox[{"p1", "-", "p0"}], ")"}]}]}], "//", "FullSimplify"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"B2", "[", 
    RowBox[{"t_", ",", "p0_", ",", "p1_", ",", "p2_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}], "*", 
      RowBox[{"B1", "[", 
       RowBox[{"t", ",", "p0", ",", "p1"}], "]"}]}], "+", 
     RowBox[{"t", "*", 
      RowBox[{"B1", "[", 
       RowBox[{"t", ",", "p1", ",", "p2"}], "]"}]}]}], "//", 
    "FullSimplify"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"B3", "[", 
    RowBox[{"t_", ",", "p0_", ",", "p1_", ",", "p2_", ",", "p3_"}], "]"}], ":=", 
   RowBox[{
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"1", "-", "t"}], ")"}], "*", 
      RowBox[{"B2", "[", 
       RowBox[{"t", ",", "p0", ",", "p1", ",", "p2"}], "]"}]}], "+", 
     RowBox[{"t", "*", 
      RowBox[{"B2", "[", 
       RowBox[{"t", ",", "p1", ",", "p2", ",", "p3"}], "]"}]}]}], "//", 
    "FullSimplify"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"B1", "[", 
  RowBox[{"t", ",", "p0", ",", "p1"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{"B2", "[", 
  RowBox[{"t", ",", "p0", ",", "p1", ",", "p2"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{"B3", "[", 
  RowBox[{"t", ",", "p0", ",", "p1", ",", "p2", ",", "p3"}], "]"}]}], "Input",\

 CellChangeTimes->{{3.631821663865678*^9, 3.631821695856192*^9}, {
  3.631821728222472*^9, 3.6318218337218227`*^9}}],

Cell[BoxData[
 RowBox[{"p0", "-", 
  RowBox[{"p0", " ", "t"}], "+", 
  RowBox[{"p1", " ", "t"}]}]], "Output",
 CellChangeTimes->{{3.6318217952938128`*^9, 3.631821834602894*^9}}],

Cell[BoxData[
 RowBox[{"p0", "+", 
  RowBox[{"2", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "p0"}], "+", "p1"}], ")"}], " ", "t"}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"p0", "-", 
     RowBox[{"2", " ", "p1"}], "+", "p2"}], ")"}], " ", 
   SuperscriptBox["t", "2"]}]}]], "Output",
 CellChangeTimes->{{3.6318217952938128`*^9, 3.631821834607045*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "p0"}], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "1"}], "+", "t"}], ")"}], "3"]}], "+", 
  RowBox[{"t", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"3", " ", "p1", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"-", "1"}], "+", "t"}], ")"}], "2"]}], "+", 
     RowBox[{"t", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"3", " ", "p2"}], "-", 
        RowBox[{"3", " ", "p2", " ", "t"}], "+", 
        RowBox[{"p3", " ", "t"}]}], ")"}]}]}], ")"}]}]}]], "Output",
 CellChangeTimes->{{3.6318217952938128`*^9, 3.6318218347235518`*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Poisson normalisation", "Section",
 CellChangeTimes->{{3.631916063722327*^9, 3.631916066346183*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"PDF", "[", 
   RowBox[{
    RowBox[{"PoissonDistribution", "[", "\[Lambda]", "]"}], ",", "N"}], "]"}],
   "//", "FullSimplify"}]], "Input",
 CellChangeTimes->{{3.631916069263219*^9, 3.631916087122154*^9}, {
  3.631916352606518*^9, 3.631916361209743*^9}, {3.631916898624462*^9, 
  3.631916899100608*^9}}],

Cell[BoxData[
 TagBox[GridBox[{
    {"\[Piecewise]", GridBox[{
       {
        FractionBox[
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"-", "\[Lambda]"}]], " ", 
          SuperscriptBox["\[Lambda]", "N"]}], 
         RowBox[{"N", "!"}]], 
        RowBox[{"N", "\[GreaterEqual]", "0"}]},
       {"0", 
        TagBox["True",
         "PiecewiseDefault",
         AutoDelete->True]}
      },
      AllowedDimensions->{2, Automatic},
      Editable->True,
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxItemSize->{
       "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.84]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}},
      Selectable->True]}
   },
   GridBoxAlignment->{
    "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
     "RowsIndexed" -> {}},
   GridBoxItemSize->{
    "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
     "RowsIndexed" -> {}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[0.35]}, 
       Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}, "RowsIndexed" -> {}}],
  "Piecewise",
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True,
  Selectable->False]], "Output",
 CellChangeTimes->{
  3.6319160879703627`*^9, {3.631916356207629*^9, 3.631916361956398*^9}, 
   3.631916899692584*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "[", "\[Lambda]_", "]"}], "=", 
  RowBox[{"PDF", "[", 
   RowBox[{
    RowBox[{"PoissonDistribution", "[", "\[Lambda]", "]"}], ",", 
    "\[Lambda]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6319164903580503`*^9, 3.631916496008409*^9}}],

Cell[BoxData[
 TagBox[GridBox[{
    {"\[Piecewise]", GridBox[{
       {
        FractionBox[
         RowBox[{
          SuperscriptBox["\[ExponentialE]", 
           RowBox[{"-", "\[Lambda]"}]], " ", 
          SuperscriptBox["\[Lambda]", "\[Lambda]"]}], 
         RowBox[{"\[Lambda]", "!"}]], 
        RowBox[{"\[Lambda]", "\[GreaterEqual]", "0"}]},
       {"0", 
        TagBox["True",
         "PiecewiseDefault",
         AutoDelete->True]}
      },
      AllowedDimensions->{2, Automatic},
      Editable->True,
      GridBoxAlignment->{
       "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
        "RowsIndexed" -> {}},
      GridBoxItemSize->{
       "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[0.27999999999999997`], {
           Offset[0.84]}, 
          Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.2], {
           Offset[0.4]}, 
          Offset[0.2]}, "RowsIndexed" -> {}},
      Selectable->True]}
   },
   GridBoxAlignment->{
    "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
     "RowsIndexed" -> {}},
   GridBoxItemSize->{
    "Columns" -> {{Automatic}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.}}, 
     "RowsIndexed" -> {}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[0.35]}, 
       Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}, "RowsIndexed" -> {}}],
  "Piecewise",
  DeleteWithContents->True,
  Editable->False,
  SelectWithContents->True,
  Selectable->False]], "Output",
 CellChangeTimes->{3.631916497165906*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"f", "[", "\[Lambda]", "]"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Lambda]", ",", "0", ",", "9"}], "}"}], ",", 
   RowBox[{"PlotRange", "\[Rule]", "All"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.631916373233974*^9, 3.631916379673773*^9}, {
  3.631916416114402*^9, 3.631916432020689*^9}, {3.631916501453352*^9, 
  3.6319165090986347`*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV1Hc41f0fBnA7s0iRVb6SBlFWSnV/SrtEHiqjUpFKRkgqSaWSKE9CoUJC
oVJC5jlWyMjee52DYx4eo/j5/XFf9/X6431d779u6pyDkTUXBwfH+4X8v9lX
1wxORXvvzHRpb56eH0aOV8RRmdcXwJPt22XpNIzr8Z85+sNc4eshJubEO4z9
hxdtfDfhiQa35yra74dgREs2rQh7CP47eyeO6Q/hlNaFh/N6vhAOTPSLm2VB
s8anIWjCHy5cbipcn1nAoW18+WFBMOPLkfKhWMjjeS5r4fgK4XP3uhLaBnEo
m6k+rheGMldhK//wQSiHlHz2nwiH2mVvg1vrB6G9xL05Kywa9gGOzd5GA7gx
GGPjwxeLQ5nhm37LDyCjqGrMxPED0r72yriP9GOXl7IASy8e7XmhivEB/Yh2
Z4X4TXyBkZXefa8+Jg7Y3rd0/piIkA1eQ+wsJpimUkqmZ75ic/bNuUPBTKhs
2ZuoWPQNaqmpWncOMZE4GvYzPSwZnuuCKLE0Boza1f0ijqWAT87eLzGYAXZZ
odEjvlQMtR5TmrzGgHY8u8XI8QeK377aZazJQMaFI2ymXgYKywXbfqf0oZt1
W9R3goa6QfFbvLW90Oo5MD5nQMeDGP/FSem9eNgsXnv1Ix2+MVn8+RG9WPvr
Q+iJMzmY8dwcz+3Qi8uxNUqri3LRZ2bK+CzSi+FzG3ekhRVgpd6OiGHTHrzU
6y4qnCuAhLmEJnt3D4hi6PE6y5+o1zL026fSg+c9/I5sxUIEc7AOlM53Q/Ni
d6RqQhFGREwsND52w80ulP9dZgmGzc/Tlgp1Q/6oUWDiqlIoihV81p7qQpGq
gALtbin+Oxxt+m9PF6RHXHVb9pahmf+5Xy+tCxlORvaSZeVwuXWck32jCxwZ
PP17HSpgHJC+vozdCWMVy0364RXgkj2/b6C7E7Fh6a7GFRUweCA0uammE8du
O3Gf16jE8qvcBzSSOxG5s13W879KJGeObOe82Yk99HSDtDvVGCuKstfl74R3
gdN3Vf86xJ/b5ROi2wEpy1O6k5l1GGJGh3ps6sDH6f30zIE61OZd03Vb04ES
FbnSI/vr4feqXihuSQfEAn52X56rR1n67O1FPe0IOSO7LOZKIwbm+mbPB7Qj
birfadXBFqh0zJTvn2nD9udfpnpdWxDzITLlCaMNZcqhHp+iWhC5N3kJq7YN
I6cdfXZwtOK+UEYRX1IbNPOlI81TWsE6YZXi59CG1sIUxq+l7RBQDu+IGWiF
TaDwCv67C3+0hWDLZAss9XiZbsEdEGkMecHoa4HZ6N8fzIQOpFM7j35qaMER
/WHzX40d2D3nt9E1swXqvJVvn2p2Ii/CzmnsQQvmXILXLmd0QvzJr7Rr0i0I
NFbYomDQjY7tK1K9DZvxlEuGP8C6G9umTR2d9zTD+4t4A7d7N/KvP31kp9OM
m8K8t3piu7HuTZbhM/lmWOb1ZcVy9UDBiV79YLQJKpoJ+9SSe7CRP4JbMagJ
ueI6x7fL9YHpeFvhLLMRSzPixF5o9cGM/4zTytZGnLNaVTqg34cAk/xiZmUj
OL7z7Qn16EOSuAD1MaMRO4/XbJ5p68PTXNGDZf6NSHvpJJIayYAzKfrhq9uI
L3IJeRrr+iH9612cekgDOArkPZ+QfkS8n3Hy8G+Agf0L3a6T/XAaOqFa87AB
Q1m3vj5/3I+g3evV3jk3QNnyUPhofz+0XzOUsvUbEB3Zd+tzwgCSnCN4erkb
ELZWQUNZY2GX/Ok1V6/Xw8fq1OKugywcK3F55u1QD7eIl8wQy4Udy+zvi7ap
xz8yS8IFn7JwI8Blz8yJeggu+SPcz2Aha4CcmtGpx43Jmt7YN0PQP76TZfKn
DnGde/TFeUcwKp3oqepdB+/EKY5/HUdh41dnfPhrLTaUVp1vdx/FelmjHVfj
alHC+FSg9ngUuYtzDV5H1UJU3tq3LGIU86HFpYuCaxH8tEJSuGrBfy9tXuNe
i2jbj6qPtMbAObto4Mb+WuStsbC4PTMG/7ePjI511oAzJDvlkhcbPwQczsqs
rcH2C2ERd/zZEMLlpBiqBq7qN54EhrHBe1dCR0e2Bv3F6mfoSWyky27IsBar
QfXsez6pHjYaqnpWj81WI/qUr0nh3gl8KBCY+FVRjSOU6bjSoklw5PHePetZ
jeDYMdXux/9h28lQTjlGFWYOrXh7znwGgdEW6sqDlZDVeX0669JffM1wCRLj
rMQDCx+hJQocRINHO61qVwUEN/35ucSbk9Rbj1Fxob8xHCunckeYm6RMbVzM
nihHmm2q74grD+FxOSL1jVGG7iquwN/zvETs5tp39e2lkDO4f/vu3UXEsM2V
p2OiBDuW3T6nyeQndsqGlPLGEmR4KA2bOAgSQ9+cE+1Wv0BpHHS70SREHKff
lGxJLUavr8wHoQsipF4y6k+xYDF+TK9Os2cuJgnKAUUZV4qwtb6y69J6UZLI
z82a6SzEvoe2Wf+wRMnHBsUpsRMLbiDBO6PEyEDCq+wrzT9RprW5/d8TS4nQ
Y90X45d/gtd62l1WWpz4eUlq/5gsgNrWOcnYcnESYbPv5Wr/Agi0N/LZvlhG
UkWSvpQoFeCyf6V1kMlykmBt21qcl4/sHr7cHAEJ0uxU/7fcLh+5IuIuSqUS
RBNfHEbE85HWaL76yX1JIntgy1tGbh62JmbvPqm9glzLL0sud8uDjdnsd4up
FWRCTXVpxYY8PI51XnsuQYpMTZnckerJhVlR7uCbq9KEN5G2821YLlY9M/zS
vE6GWGkVzH4wz4Xt6amue80yJLnwjv+plbmw4XCztXwrS9aOq24725qD4/s1
R3eayBF7/W/lrVE5YFsWFhtLryTlHiO25RdzIO11fVlV00rSplkpP6+WA40j
5y2M/FeRV3ZmO8r/0OHhLO7Os0GeRG1q+678kw7zAPGDlg/kyc2zCt+yg+j4
s4U6OtIqTwRfjHhkX6RjIl/6rwyhiH+nlMkFHTouRqsrF4ZQZOu1hLJ4YTpq
Ne0ET4VR5JkI+96gEB16uTHM0dcUkfSJMFRZ8Mp22ViZCIoU10jrxAvQUS21
aI1DDEU8I/u1E/jogF/TSokkilzhVhz+xkGHhIvXUqtSisyd9lhdzabBiyt7
dKqMIk+WDk1LLHjUf/q332+KfF++nGk6TkNJgv2zlCqKDD1yFmsfpeFe30lh
oUaKbO9qeswaomHIbCPf116KMPk+LRNh0mDBtOnZx1i4zxB1Ocagoeh6ZF4T
kyLrnGsGAvtoiHoheZ+XRZGoAgfFVb00mJVxcpiOU+RWpPEP9S4aCi22t7HY
FHk4qNF0vZMGrQHXrHuTFBHdcHh5ZgcNovyD7vHTFNEIFq7c307D7SAli12z
FPE7Hm/s10ZDv+JZ3do/FGld5zBc2UrDyW+h0rZzFBlvMIhcseD8XbXT8/MU
Udty0O50Cw3/AzFDJfs=
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0.1},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{"DefaultBoundaryStyle" -> Automatic, "ScalingFunctions" -> None},
  PlotRange->{All, All},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{
  3.631916380872623*^9, {3.631916419915246*^9, 3.631916432566416*^9}, {
   3.631916505849127*^9, 3.631916509461575*^9}}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 701},
WindowMargins->{{161, Automatic}, {36, Automatic}},
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (June 27, \
2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 109, 1, 64, "Section"],
Cell[CellGroupData[{
Cell[714, 27, 1690, 50, 131, "Input"],
Cell[2407, 79, 177, 4, 28, "Output"],
Cell[2587, 85, 370, 11, 32, "Output"],
Cell[2960, 98, 671, 22, 35, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3680, 126, 106, 1, 64, "Section"],
Cell[CellGroupData[{
Cell[3811, 131, 338, 8, 28, "Input"],
Cell[4152, 141, 1828, 53, 59, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6017, 199, 277, 7, 28, "Input"],
Cell[6297, 208, 1775, 51, 59, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8109, 264, 400, 9, 28, "Input"],
Cell[8512, 275, 4476, 85, 237, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
