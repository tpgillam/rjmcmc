#!/usr/bin/env python

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
from mcmcthread import MCMCThread
from updatinggui import UpdatingGUI
from scipy.stats import binom


# class PlotWidgetWithKeyboard(pg.PlotWidget):
#     def __init__(self):
#         pg.PlotWidget.__init__(self)

#     def keyPressEvent(self, e):
#         quit_keys = [QtCore.Qt.Key_Escape, QtCore.Qt.Key_Q]
#         if e.key() in quit_keys:
#             app = QtGui.QApplication.instance()
#             app.exit()


class CoinTossParams:
    """
    fair: if True indicates that prob of getting head is exactly 0.5
    p:    if fair is False, this is the probability of getting a head
    """

    def __init__(self):
        self.fair = True
        self.p = 0

    def __repr__(self):
        if self.fair:
            return 'FAIR'
        else:
            return 'UNFAIR: {:.3f}'.format(self.p)


class CoinTossMCMC(MCMCThread):
    def __init__(self, num_observed_heads, num_observed_tails, params=None):
        if params is None:
            params = CoinTossParams()
        self.num_observed_heads = num_observed_heads
        self.num_observed_tails = num_observed_tails
        self.total_fair = 0
        self.total_unfair = 0
        self.p_draws = []
        super().__init__(params)

    def propose_step(self):
        """
        Propose a metropolis-hastings step in the parameter space. Return both
        the new parameters and the logarithm of the qfactor ratio associated
        with the step.
        """
        # Propose a step
        proposal = CoinTossParams()
        if self.curr_params.fair:
            # We *must* propose a move to an unfair coin
            proposal.fair = False
            # Choose p randomly...
            proposal.p = np.random.uniform(0, 1)
            prob_this_step = 1/1
            prob_inverse_step = 0.5
        else:
            # With probability 0.5 choose propose moving around in this space,
            # otherwise propose moving back to a fair coin
            if np.random.rand(1) < 0.5:
                proposal.fair = False
                proposal.p = np.random.uniform(0, 1)
                prob_this_step = 0.5 * 1/1
                prob_inverse_step = 0.5 * 1/1
            else:
                proposal.fair = True
                prob_this_step = 0.5
                prob_inverse_step = 1 / 1
        log_qfactor = np.log(prob_inverse_step) - np.log(prob_this_step)
        return proposal, log_qfactor

    def compute_log_prob(self, params):
        """
        Compute a quantity proportional to the target probability distribution,
        returning the logarithm of the density.
        """
        # Equal probability of two hypotheses a-priori
        prior_prob_fair = 0.5
        if params.fair:
            p = 0.5
            prior_factor = prior_prob_fair
        else:
            p = params.p
            # Uniform prior over p when unfair coin
            prior_factor = (1 - prior_prob_fair) * 1 / 1
        x = self.num_observed_heads
        n = self.num_observed_heads + self.num_observed_tails
        log_likelihood = binom.logpmf(x, n, p)
        return log_likelihood + np.log(prior_factor)

    def every_step_callback(self):
        """Will be called after every proposal."""
        if self.curr_params.fair:
            self.total_fair += 1
        else:
            self.total_unfair += 1
            self.p_draws.append(self.curr_params.p)


class CoinTossGUI(UpdatingGUI):
    def __init__(self, num_heads, num_tails):
        self.num_heads = num_heads
        self.num_tails = num_tails
        super().__init__(
                title='Coin toss experiment',
                width=800,
                height=400)

    def populate_gui(self):
        """Create plots and other things in the GUI"""
        self.plot1 = self.win.addPlot()
        self.plot2 = self.win.addPlot()

        self.plot1.disableAutoRange()
        self.plot1.setRange(xRange=(0,1), yRange=(0,1))
        self.bins1 = [0, 0.5, 1]
        self.hist_curve1 = pg.PlotCurveItem(
                self.bins1, [0]*(len(self.bins1)-1), 
                stepMode=True, 
                fillLevel=0, 
                pen=None,
                brush='y')
        self.plot1.addItem(self.hist_curve1)

        self.plot2.enableAutoRange()
        self.plot2.setRange(xRange=(0,1))
        self.bins2 = np.linspace(0, 1, 50)
        self.hist_curve2 = pg.PlotCurveItem(
                self.bins2, [0]*(len(self.bins2)-1), 
                stepMode=True, 
                fillLevel=0, 
                pen=None,
                brush='y')
        self.plot2.addItem(self.hist_curve2)

        self.label_eff = pg.TextItem('eff = ')
        self.plot1.addItem(self.label_eff)
        self.label_eff.setPos(0.1, 1.0)


    def create_worker(self):
        """Return an instance of the worker thread object"""
        return CoinTossMCMC(self.num_heads, self.num_tails)

    def update(self):
        """Refresh data in any plots"""
        self.worker.lock()
        y1 = (np.array([self.worker.total_fair, self.worker.total_unfair]) /
                self.worker.num_proposals)
        p_draws = self.worker.p_draws
        self.worker.p_draws = []
        self.worker.unlock()

        this_p_hist,_ = np.histogram(p_draws, bins=self.bins2)
        if not 'p_hist' in self.__dict__:
            self.p_hist = this_p_hist
        else:
            self.p_hist += this_p_hist
        float_hist = self.p_hist.astype(float)
        float_hist /= np.sum(float_hist)

        self.hist_curve1.setData(self.bins1, y1)
        self.hist_curve2.setData(self.bins2, float_hist)

        self.label_eff.setText('eff = {:.2f}%'.format(100.0
            * self.worker.num_accepted / self.worker.num_proposals))


if __name__ == '__main__':
    gui = CoinTossGUI(100, 100)
    gui.run()
