#!/usr/bin/env python

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import scipy.optimize
from mcmcthread import MCMCThread
from updatinggui import UpdatingGUI
import itertools
import copy
import math


# TODO Control parameters that should be put somewhere more sensible
fitting_stddev = 0.08
bendiness_width_rad = np.pi / 10
line_width = fitting_stddev / 10
new_point_min_distance_from_line = line_width / 10


def pairwise(iterable):
    """
    Iterate over pairwise elements of list, i.e.
    s -> (s0,s1), (s1,s2), (s2, s3), ...
    """
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def delta_phi(x, y):
    """
    Return the smallest opening angle between angles x and y, given that each
    are in the range [-pi, pi].
    """
    return min(y-x, y-x+2*np.pi, y-x-2*np.pi, key=abs)


def log_choose(n, x):
    """
    Compute the logarithm of the combinatoric n choose x factor, which should be
    robust for large n & x.
    """ 
    return math.lgamma(n+1) - (math.lgamma(x+1) + math.lgamma(n - x + 1))


class CubicBezierPointGenerator:
    """
    Generate a random cubic Bezier curve, and then throw points at any of the
    x-values contained in x_slices which satisfy min_x and max_x.
    """
    def __init__(self, x_slices, min_x=0.1, max_x=0.9):
        self.x_slices = x_slices
        # self.noise = 0.005
        self.noise = 0
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = 0
        self.max_y = 1
        self._generate_control_points()

    def _generate_control_points(self):
        ctrl_x = np.sort(np.random.uniform(self.min_x, self.max_x, 4))
        # Enforce a maximum span in x for testing
        ctrl_x[0] = self.min_x
        ctrl_x[-1] = self.max_x
        ctrl_y = np.random.uniform(self.min_y, self.max_y, 4)
        self.ctrl = np.transpose([ctrl_x, ctrl_y])

    def evaluate_param(self, t):
        return self.ctrl[0]*(1 - t)**3 + t*(3*self.ctrl[1]*(-1 + t)**2 + 
                t*(3*self.ctrl[2] - 3*self.ctrl[2]*t + self.ctrl[3]*t))

    def draw_samples(self):
        # Determine the t_values required to linearly space in *x*
        x_vals = [x for x in self.x_slices 
                if x >= self.min_x and x <= self.max_x]
        t_vals = []
        for x_val in x_vals:
            def test_func(t):
                point = self.evaluate_param(t)
                return point[0] - x_val
            t_vals.append(scipy.optimize.newton(test_func, 0.5))
        points = np.array([self.evaluate_param(t_val) for t_val in t_vals])
        if self.noise > 0:
            points += np.random.normal(0, self.noise, np.shape(points))
        return points


class CurveFitParams:
    """
    MCMC to fit a single line based on a set of data. The model assumes that all
    points given are *either* part of a smooth line, *or* come from a uniformly
    distributed noise source.
    """

    def __init__(self):
        # This should be an *ordered* list of points, by x-value. There should
        # never be only one point in this line (zero is allowed if there is
        # currently no line)
        self.points = np.array([])

    def min_x(self):
        if len(self.points) < 2:
            return None
        return self.points[0][0]

    def max_x(self):
        if len(self.points) < 2:
            return None
        return self.points[-1][0]

    def add_point(self, point):
        """
        Add a point to the line segment. Will do it such that ordering is
        preserved.
        """
        if len(self.points) == 0:
            self.points = np.array(point)
            self.points = self.points.reshape((1,2))
            return
        insertion_loc = np.apply_along_axis(
                lambda line_points: line_points.searchsorted(point[0]),
                axis=0,
                arr=self.points)[0]
        self.points = np.insert(self.points, 
                insertion_loc,
                point,
                axis=0)

    def remove_point(self, index):
        """Remove the point at the specified index."""
        self.points = np.delete(self.points, index, axis=0)

    def y_value_on_line(self, x):
        """
        Return the y value lying on the appropriate segment of the line given an
        x-value. It will extrapolate linearly if the x value does not lie within
        the specified range of the line.
        """
        # Find the points to the left and right of the test x-value
        right_index = np.apply_along_axis(
                lambda line_points: line_points.searchsorted(x),
                axis=0,
                arr=self.points)[0]
        if right_index == len(self.points) and len(self.points) > 0:
            # The test point is off the RHS -- extrapolate
            right_index = -1
            left_index = -2
        elif right_index > 0:
            # The test point is in the middle of the line
            left_index = right_index - 1
        elif len(self.points) > 1:
            # The test point is off the LHS --extrapolate
            left_index = 0
            right_index = 1
        else:
            raise RuntimeError('points has invalid length {}'.format(
                len(self.points)))
        point_left = self.points[left_index]
        point_right = self.points[right_index]
        return self._linear_interpolate(point_left, point_right, x)

    def y_distance_to_point(self, point):
        """
        Return the separation in y-direction between the given point and the
        current line.
        """
        return abs(point[1] - self.y_value_on_line(point[0]))

    def _linear_interpolate(self, point0, point1, x):
        """
        Interpolate linearly between point0 and point1, and return the y value
        corresponding to the given x value. Assume that both point0 and point
        are 2 element arrays/lists.
        """
        if point1[0] == point0[0]:
            raise RuntimeError("Should not have two points in same slice")
        m = (point1[1] - point0[1]) / (point1[0] - point0[0])
        c = point0[1] - m*point0[0]
        return m*x + c

    def coords(self):
        """Return x and y coordinates."""
        temp = np.transpose(self.points)
        return (temp[0], temp[1])


class CurveFitMCMC(MCMCThread):
    def __init__(self, data_points):
        self.data_points = data_points
        init_params = CurveFitParams()
        super().__init__(init_params)

    def propose_step(self):
        """
        Propose a metropolis-hastings step in the parameter space. Return both
        the new parameters and the logarithm of the qfactor ratio associated
        with the step.
        """
        # TODO
        # Have subclass of MCMCThread which keeps N maximum a posteriori
        # parameter values, which might be useful at the end.

        # Start from current position
        proposal = copy.deepcopy(self.curr_params)

        # If have a line that *could be removed*, then what do we do...
        options = {
                'REMOVE': 0.4,
                'ADD': 0.6,
                }
        options_keys = list(options.keys())
        options_probs = [options[key] for key in options_keys]

        if len(self.curr_params.points) == 0:
            # MUST propose new line, do so randomly between two points
            two_indices = np.random.choice(len(self.data_points), 2, replace=False)
            point0 = self.data_points[two_indices[0]]
            point1 = self.data_points[two_indices[1]]
            proposal.add_point(point0)
            proposal.add_point(point1)
            # Take into account degeneracy of selecting many different possible lines
            log_prob_this_step = - log_choose(len(self.data_points), 2)
            log_prob_inverse_step = np.log(options['REMOVE'])
        elif len(self.curr_params.points) >= 2:
            option = np.random.choice(options_keys, 1, p=options_probs)[0]
            if option == 'REMOVE':
                if len(self.curr_params.points) == 2:
                    proposal.remove_point(0)
                    proposal.remove_point(0)
                    # Take into account ability to select many types of line
                    log_prob_this_step = np.log(options['REMOVE'])
                    # This is *right* because all points are fair game at this
                    # stage. This MIGHT NOT BE TRUE in the multi curve case??
                    log_prob_inverse_step = - log_choose(len(self.data_points), 2)
                else:
                    num_points = len(proposal.points)
                    num_points_after_remove = num_points - 1
                    index = np.random.choice(num_points, 1)
                    proposal.remove_point(index)
                    log_prob_this_step = np.log(options['REMOVE']) - np.log(num_points)
                    # Take into account plausible candidate additions after
                    # having removed the point
                    num_candidate_points = self._num_candidate_points(proposal)
                    log_prob_inverse_step = (np.log(options['ADD']) 
                            - np.log(num_candidate_points))
            elif option == 'ADD':
                # Pick random point that isn't *close* to any existing points
                candidate_points = self._compute_candidate_points(proposal)
                index_to_add = np.random.choice(len(candidate_points), 1)[0]
                proposal.add_point(candidate_points[index_to_add])
                log_prob_this_step = (np.log(options['ADD']) 
                    - np.log(len(candidate_points)))
                log_prob_inverse_step = (np.log(options['REMOVE']) 
                    - np.log(len(proposal.points)))
            else:
                raise RuntimeError('Unrecognised option')
        else:
            raise RuntimeError("Shouldn't be able to get here -- indicates \
                    malformed parameter set.")

        log_qfactor = log_prob_inverse_step - log_prob_this_step
        return proposal, log_qfactor

    def _compute_candidate_points(self, params):
        """
        Given a current state, enumerate the proposals that we are *allowed* to
        make.
        """
        candidate_points = []
        for point in self.data_points:
            if (params.y_distance_to_point(point) >
                    new_point_min_distance_from_line):
                candidate_points.append(point)
        return candidate_points

    def _num_candidate_points(self, params):
        return len(self._compute_candidate_points(params))

        # TODO types of step that can be made:
        # 
        # -- adding new line [in *this* case, only applicable if there
        #                     is not an existing line]
        #
        # -- removing a line
        #
        # -- adding a point to an existing line
        #           * add to front, back, or middle
        #           * where exactly to place?
        #
        # -- removing a point from an existing line [must have > 1 point]. 
        #           * remove from front, back, or middle?
        #
        # -- moving an existing point on a line
        #
        # [For when using multiple lines]
        # -- joining two existing line segments
        # -- cutting one line segment
        #

        # TODO thoughts on how steps should work
        # might want proposal of removing points within lines ... but that means
        # we need proposal of points within lines too.
        # when proposing points at the end of lines, 
        #   ** most likely just make a small increment (1 temperature step)
        #   ** have a long tail to make larger temperature steps
        #   ** when step draw the *angle* too, so most likely to go straight,
        #   but might not. - should also be influenced by neighbouring
        #   temperature steps (so adjacent curves follow similar shape?)


    def compute_log_prob(self, params):
        """
        Compute a quantity proportional to the target probability distribution,
        returning the logarithm of the density.
        """
        # Split data points into that associated with line and those not
        line_indices = list(self._generate_indices_for_line(params))
        unassociated_indices = [x for x in range(len(self.data_points)) 
                if x not in line_indices]
        log_likelihood = 0

        # Contribution from points on the line
        for line_index in line_indices:
            point = self.data_points[line_index]
            distance = params.y_distance_to_point(point)
            log_likelihood += - distance**2 / ( 2 * fitting_stddev**2 )

        # Contribution from N unassociated points
        #
        # Int P(N | \lambda) P(\lambda) d \lambda
        #
        # TODO
        # TODO
        # TODO
        # NB -- here we are assuming an improper flat P(lambda) prior in the
        # number of noise events. So, 
        N_unassociated = len(unassociated_indices)
        # Volume represents space over which points would be randomly
        # distributed
        # FIXME
        # FIXME
        # FIXME
        # FIXME
        volume = 1 * 1
        log_likelihood += - N_unassociated * np.log(volume)

        # Now work out prior, which will be based on line "bendiness"
        log_prior = 0
        for angle in self._compute_line_angles(params):
            log_prior += - (angle)**2 / ( 2 * bendiness_width_rad**2 )

        return log_likelihood + log_prior

    def _debug_angle_prior(self, params):
        debug_string = ''
        for angle in self._compute_line_angles(params):
            debug_string += '{0:.2f}[{1:.2f}], '.format(
                    - (angle)**2 / ( 2 * bendiness_width_rad**2 ),
                    angle
                    )
        return debug_string

    def _generate_indices_for_line(self, line_params):
        """
        Return the indices of the data points in the test set associated with
        the line specified by the line_params argument.
        """
        line_min_x = line_params.min_x()
        line_max_x = line_params.max_x()
        if line_min_x is None or line_max_x is None:
            return    
        for i,point in enumerate(self.data_points):
            if not (point[0] >= line_min_x and point[0] <= line_max_x):
                continue
            line_y = line_params.y_value_on_line(point[0])
            if abs(line_y-point[1]) < line_width:
                yield i

    def _compute_line_angles(self, line_params):
        """
        For a given line, generate the full set of angles. For a line with 0
        points this will generate 0 angles; for a line with N > 1 points it will
        generate N-2 angles.
        """
        for angle0,angle1 in pairwise(self._generate_line_angles(line_params)):
            yield delta_phi(angle1, angle0)

    def _generate_line_angles(self, line_params):
        """
        Generate the absolute angles of each line relative to the horizontal.
        Is in range [-pi, pi]. Assume that points in line_params are sorted by
        x co-ordinate.
        """
        for p0,p1 in pairwise(line_params.points):
            yield np.arctan2(p1[1]-p0[1], p1[0]-p0[0])


class CurveFitGUI(UpdatingGUI):
    def __init__(self, num_random_test=4):
        self.num_random_test = num_random_test
        self._generate_test_points()
        super().__init__(
                title='Curve fitting experiment',
                width=500,
                height=400)

    def _generate_test_points(self):
        # Specify the temperature discretisation
        self.x_slices = np.linspace(0, 1, 20)

        point_generator = CubicBezierPointGenerator(self.x_slices)
        self.test = point_generator.draw_samples()

        # Add some uniformly distributed random points
        noise_x = np.random.choice(self.x_slices, self.num_random_test)
        noise_y = np.random.uniform(0, 1, self.num_random_test)
        noise = np.transpose([noise_x, noise_y])
        self.test = np.append(self.test, noise, axis=0)

        test_transpose = np.transpose(self.test)
        self.test_x = test_transpose[0]
        self.test_y = test_transpose[1]

    def populate_gui(self):
        """Create plots and other things in the GUI"""
        self.plot1 = self.win.addPlot()
        self.plot1.disableAutoRange()
        self.plot1.setRange(xRange=(0,1), yRange=(0,1))

        self.x_slice_indicators = []
        for x in self.x_slices:
            line = pg.InfiniteLine(x, 
                    pen=pg.mkPen(color=(20,20,100), style=QtCore.Qt.DashLine, alpha=0.3))
            self.x_slice_indicators.append(line)
            self.plot1.addItem(line)

        self.test_plot = self.plot1.plot(
                x=self.test_x, y=self.test_y,
                pen=None, symbol='x', symbolBrush='c', symbolPen='c')
        self.test_fitted_plot = self.plot1.plot(
                pen=None, symbol='x', symbolBrush='r', symbolPen='r')

        self.line_plot = self.plot1.plot(
                pen='y', symbol='o', symbolBrush='y')

        self.label_eff = pg.TextItem('eff = ')
        self.plot1.addItem(self.label_eff)
        self.label_eff.setPos(0.0, 1.0)

        self.label_debug = pg.TextItem('debug')
        self.plot1.addItem(self.label_debug)
        self.label_debug.setPos(0.0, 0.92)

    def create_worker(self):
        """Return an instance of the worker thread object"""
        return CurveFitMCMC(self.test)

    def update(self):
        """Refresh data in any plots"""
        self.worker.lock()
        curr_params = self.worker.curr_params
        self.worker.unlock()

        line_x,line_y = curr_params.coords()
        self.line_plot.setData(x=line_x, y=line_y)

        fitted_indices = list(self.worker._generate_indices_for_line(
            self.worker.curr_params))
        unfitted_indices = [x for x in range(len(self.test)) 
                if x not in fitted_indices]
        fitted_points = self.test[fitted_indices]
        unfitted_points = self.test[unfitted_indices]
        unfitted_x = np.transpose(unfitted_points)[0]
        unfitted_y = np.transpose(unfitted_points)[1]
        fitted_x = np.transpose(fitted_points)[0]
        fitted_y = np.transpose(fitted_points)[1]
        self.test_plot.setData(x=unfitted_x, y=unfitted_y)
        self.test_fitted_plot.setData(x=fitted_x, y=fitted_y)

        # self.label_debug.setText(self.worker._debug_angle_prior(curr_params))
        self.label_debug.setText('')

        self.label_eff.setText('eff = {:.2f}%'.format(100.0
            * (self.worker.num_accepted / self.worker.num_proposals)))


if __name__ == '__main__':
    # np.random.seed(0)
    # np.random.seed(1)
    np.random.seed(2)
    gui = CurveFitGUI(num_random_test=10)
    gui.run()

