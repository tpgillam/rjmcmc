from pyqtgraph.Qt import QtGui, QtCore
import numpy as np


class MCMCThread(QtCore.QThread):
    def __init__(self, init_params):
        QtCore.QThread.__init__(self)
        self.quitting = False
        self.num_proposals = 0
        self.num_accepted = 0
        self.curr_params = init_params
        self.curr_log_prob = self.compute_log_prob(self.curr_params)
        self.mutex = QtCore.QMutex()

    def __del__(self):
        self.quitting = True
        self.wait()

    def run(self):
        while not self.quitting:
            # This seems to be necessary to prevent blocking of
            # GUI thread
            app = QtGui.QApplication.instance()
            if app is not None:
                app.processEvents()
            self.lock()
            accepted = self.try_proposal()
            self.num_proposals += 1
            if accepted:
                self.num_accepted += 1
                self.accepted_callback()
            else:
                self.unaccepted_callback()
            self.every_step_callback()
            self.unlock()

    def lock(self):
        self.mutex.lock()

    def unlock(self):
        self.mutex.unlock()

    def try_proposal(self):
        """
        Make a proposal step, and if that proposal is accepted update the
        parameters stored in the class, and return True. Otherwise just return
        False.
        """
        params, log_qfactor = self.propose_step()
        log_prob = self.compute_log_prob(params)
        log_prob_diff = log_prob - self.curr_log_prob
        log_transition_prob = log_prob_diff + log_qfactor
        transition_prob = min(1.0, np.exp(log_transition_prob))
        accepted = False
        if np.random.rand(1) < transition_prob:
            accepted = True
            self.curr_params = params
            self.curr_log_prob = log_prob
        return accepted

    def propose_step(self):
        """
        Propose a metropolis-hastings step in the parameter space. Return both
        the new parameters and the logarithm of the qfactor ratio associated
        with the step.
        """
        raise NotImplementedError()

    def compute_log_prob(self, params):
        """
        Compute a quantity proportional to the target probability distribution,
        returning the logarithm of the density.
        """
        raise NotImplementedError()

    def accepted_callback(self):
        """Will be called if proposal is accepted."""
        pass

    def unaccepted_callback(self):
        """Will be called if proposal is not accepted."""
        pass

    def every_step_callback(self):
        """Will be called after every proposal."""
        pass
