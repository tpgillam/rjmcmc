#!/usr/bin/env python

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np


min_param = -1
max_param = 1


def poly(a, b, c, x, i=3):
    """
    Compute:
        y(x) = a + bx + cx**2
    for an array-like x
    """
    if i == 1:
        return np.ones_like(x)*a
    elif i == 2:
        return (a + b * x )
    elif i == 3:
        return (a + b * x + c * x**2)
    else:
        raise RuntimeError('Bad value of i: {}'.format(i))
        


class PlotWidgetWithKeyboard(pg.PlotWidget):
    def __init__(self, worker_thread):
        self.worker_thread = worker_thread
        pg.PlotWidget.__init__(self)

    def keyPressEvent(self, e):
        quit_keys = [QtCore.Qt.Key_Escape, QtCore.Qt.Key_Q]
        if e.key() in quit_keys:
            app = QtGui.QApplication.instance()
            app.exit()
        elif e.key() == QtCore.Qt.Key_P:
            if not self.worker_thread.quitting:
                self.worker_thread.quitting = True
            else:
                self.worker_thread.quitting = False
                self.worker_thread.start()



class ProposalParams():
    def __init__(self):
        self.a = 0
        self.b = 0
        self.c = 0
        self.i = 3
        self.num_props = 0
        self.num_accepted = 0
        self.mutex = QtCore.QMutex()

    def lock(self):
        self.mutex.lock()

    def unlock(self):
        self.mutex.unlock()


class MCMCThread(QtCore.QThread):
    def __init__(self, prop_params, raw_x, raw_y, raw_s, plot_x):
        self.quitting = False
        self.prop_params = prop_params
        self.raw_x = raw_x
        self.raw_y = raw_y
        self.raw_s = raw_s
        self.plot_x = plot_x
        self.sum_y = np.zeros_like(plot_x)
        self.sumsq_y = np.zeros_like(plot_x)
        self.num_to_avg = 0
        self.avg_y = np.zeros_like(plot_x)
        self.up_y = np.zeros_like(plot_x)
        self.down_y = np.zeros_like(plot_x)
        self.prev_log_prob = self.compute_log_prob(
                prop_params.a, prop_params.b,
                prop_params.c, prop_params.i)
        self.num_by_i = {1:0, 2:0, 3:0}
        QtCore.QThread.__init__(self)

    def __del__(self):
        self.quitting = True
        self.wait()

    def run(self):
        np.seterr(invalid='raise')
        while not self.quitting:
            # This seems to be necessary to prevent blocking of
            # GUI thread
            app = QtGui.QApplication.instance()
            app.processEvents()
            self.make_proposal()
            self.prop_params.lock()
            self.prop_params.num_props += 1
            self.num_by_i[self.prop_params.i] += 1
            # If we've burnt in, cash information for plotting
            # Say that we've burnt in if we've moved off the start mark
            if self.prop_params.num_accepted > 10:
                self.cache_plotting_info()
            self.prop_params.unlock()

    def make_proposal(self):
        """
        Make either a 'normal' step within a given model, or propose moving to a
        new model, with some probability. Reversible jump MCMC.
        """
        # Volume over which prior in each coefficient has support -- assumed
        # uniform
        vol = max_param-min_param

        # Probability of making a "normal" jump
        p_normal = 0.8
        if np.random.rand(1) < p_normal:
            a = np.random.uniform(min_param, max_param)
            b = np.random.uniform(min_param, max_param)
            c = np.random.uniform(min_param, max_param)
            i = self.prop_params.i
            q_factor = 1.0
        else:
            # Same probability of proposing increase as
            # decrease if in middle
            if self.prop_params.i == 1:
                p_increase = 1.0
            elif self.prop_params.i == 3:
                p_increase = 0.0
            else:
                p_increase = 0.5
            if np.random.rand(1) < p_increase:
                # Increase
                i = self.prop_params.i + 1
                a = self.prop_params.a
                if i == 2:
                    b = np.random.uniform(min_param, max_param)
                    c = 0.0
                    p_reverse = 0.5
                elif i == 3:
                    b = self.prop_params.b
                    c = np.random.uniform(min_param, max_param)
                    p_reverse = 1
                else:
                    raise RuntimeError('AAARGGHH!')
                # Include prior 'volume' in q_factor
                # times P(propose coming back) / P(getting there)
                # q_factor = (1.0 / vol) *    ( ( p_reverse )  / ( p_increase * 1.0 / vol )   ) 
                q_factor = ( ( p_reverse )  / ( p_increase  )   ) 
            else:
                # Decrease
                i = self.prop_params.i - 1
                if i == 2:
                    a = self.prop_params.a
                    b = self.prop_params.b
                    c = 0.0
                    p_reverse = 0.5
                elif i == 1:
                    a = self.prop_params.a
                    b = 0.0
                    c = 0.0
                    p_reverse = 1
                else:
                    raise RuntimeError('AAARGGHH!')
                # Include prior 'volume' in q_factor
                # times P(propose coming back) / P(getting there)
                # q_factor = vol * ( ( p_reverse * (1.0 / vol) ) / ( (1-p_increase)  )   )
                q_factor =  ( p_reverse  ) / (1 - p_increase)

        # # Probability of making stupid jump
        # p_random = 0.3
        # if np.random.rand(1) < p_random:
        #     #  Stupid proposal
        #     a = np.random.rand(1)
        #     b = np.random.rand(1)
        #     c = np.random.rand(1)
        # else:
        #     # Propose around last points
        #     a = np.random.normal(self.prop_params.a, 0.03, 1)
        #     b = np.random.normal(self.prop_params.b, 0.03, 1)
        #     c = np.random.normal(self.prop_params.c, 0.03, 1)

        # Actual
        # a = 0 # b = 0.1 # c = 0.8

        log_prob = self.compute_log_prob(a, b, c, i)
        log_prob_diff = log_prob - self.prev_log_prob
        prob_ratio_trunc = min(1.0, np.exp(log_prob_diff)*q_factor)
        # Accept point with above probability
        if np.random.rand(1) < prob_ratio_trunc:
            self.prop_params.lock()
            self.prop_params.a = a
            self.prop_params.b = b
            self.prop_params.c = c
            self.prop_params.i = i
            self.prev_log_prob = log_prob
            self.prop_params.num_accepted += 1
            self.prop_params.unlock()

    def compute_log_prob(self, a, b, c, i):
        """Compute log probability of proposal, up to constant factor"""
        y = poly(a, b, c, self.raw_x, i)
        return - np.sum((y - self.raw_y)**2) / ( 2 * self.raw_s**2 )


    def cache_plotting_info(self):
        """Store information for plotting band of posterior distribution"""
        val = poly(
            self.prop_params.a,
            self.prop_params.b,
            self.prop_params.c,
            self.plot_x,
            self.prop_params.i,
            )
        self.sum_y += val
        self.sumsq_y += val**2
        self.num_to_avg += 1
        self.avg_y = self.sum_y / self.num_to_avg
        # Ensure that small values are truncated to be positive
        unc_y = np.sqrt(
                np.maximum(
                    self.sumsq_y / self.num_to_avg -
                    self.avg_y**2, 0)
                )
        self.up_y = self.avg_y + unc_y
        self.down_y = self.avg_y - unc_y
        

class ProposalPlotter():
    def __init__(self):
        self.init_points(30, 100)
        self.init_mcmc()
        self.init_gui()


    def init_points(self, num_raw_points, num_plot_points):
        """
        Start with a number of 'true' points based on a true polynomial.
        Polynomial will be of form:

            y(x) = a + bx + cx**2

        Observations of form:

            w(x) = y(x) + e,   e ~ N(0, s)

        where s is the RMS spread of the normal distribution.
        """
        self.a = -0.2
        self.b = 0.03
        self.c = 0.04
        self.s = 0.03

        self.raw_x = np.random.uniform(-1, 1, num_raw_points)
        # self.raw_x = np.linspace(-1, 1, num_raw_points)
        self.raw_y = (
                poly(self.a, self.b,
                    self.c, self.raw_x) + 
                np.random.normal(0, self.s, num_raw_points))

        # Initialise the test point
        self.prop_params = ProposalParams()

        # Initialise x-values for plotting
        self.plot_x = np.linspace(-1, 1, num_plot_points)

    def init_mcmc(self):
        self.worker_thread = MCMCThread(
                self.prop_params, 
                self.raw_x,
                self.raw_y, 
                self.s,
                self.plot_x)

    def init_gui(self):
        self.app = QtGui.QApplication([])
        self.mw = QtGui.QMainWindow()
        self.mw.setWindowTitle("Polynomial proposals")
        self.mw.resize(600, 600)
        
        self.pw = PlotWidgetWithKeyboard(self.worker_thread)
        self.mw.setCentralWidget(self.pw)

        self.mw.show()
        # Bring window to foreground
        self.mw.raise_()

        self.pw.showGrid(x=True, y=True)

        # Add proposal and average band plot items
        self.p_proposal = self.pw.plot(pen='g', symbol=None)
        self.p_avg = self.pw.plot(
                pen=pg.mkPen('y', width=1.5, style=QtCore.Qt.DashLine), 
                symbol=None)
        self.p_up = self.pw.plot(pen=pg.mkPen('y', width=1.5), symbol=None)
        self.p_down = self.pw.plot(pen=pg.mkPen('y', width=1.5), symbol=None)

        # Add error bar item
        top = np.ones_like(self.raw_x) * self.s
        bottom = top
        self.raw_err = pg.ErrorBarItem(
                x=self.raw_x, 
                y=self.raw_y, 
                top=top,
                bottom=bottom,
                beam=0.02,
                pen='r')
        self.pw.addItem(self.raw_err)

        # Add raw plot item
        self.p_raw = self.pw.plot(pen=None, symbol='x', symbolPen=None,
                symbolBrush='r')
        self.p_raw.getViewBox().disableAutoRange()
        self.p_raw.getViewBox().setRange(xRange=(-1,1), yRange=(-1,1))
        self.p_raw.setData(x=self.raw_x, y=self.raw_y)

        # Add text label to show time spent by i
        self.label1 = pg.TextItem('i = 1: ')
        self.label2 = pg.TextItem('i = 2: ')
        self.label3 = pg.TextItem('i = 3: ')
        self.label_eff = pg.TextItem('eff = ')
        self.pw.addItem(self.label1)
        self.pw.addItem(self.label2)
        self.pw.addItem(self.label3)
        self.pw.addItem(self.label_eff)
        self.label1.setPos(-0.9, 0.9)
        self.label2.setPos(-0.9, 0.83)
        self.label3.setPos(-0.9, 0.76)
        self.label_eff.setPos(-0.9, 1.0)

        # Initialise GUI update loop
        self.t = QtCore.QTimer()
        self.t.timeout.connect(self.update)
        self.t.start(20)

    def update(self):
        self.plot_proposal()

    def plot_proposal(self):
        """Plot the new proposal onto the graph"""
        self.prop_params.lock()
        a = self.prop_params.a
        b = self.prop_params.b
        c = self.prop_params.c
        i = self.prop_params.i
        # FIXME: there *could* be a deadlock here if GUI thread is terminated
        # before this is unlocked
        # if self.prop_params.num_props > 0:
        #     print('Proposed: {},    Acceptance Eff.: {:.2f}%'.format(
        #         self.prop_params.num_props,
        #         100.0 *self.prop_params.num_accepted / self.prop_params.num_props,
        #         ))
        self.label_eff.setText('eff = {:.2f}%'.format(100.0
            *self.prop_params.num_accepted / self.prop_params.num_props))
        self.label1.setText('i = 1: {}'.format(self.worker_thread.num_by_i[1]))
        self.label2.setText('i = 2: {}'.format(self.worker_thread.num_by_i[2]))
        self.label3.setText('i = 3: {}'.format(self.worker_thread.num_by_i[3]))
        self.p_avg.setData(x=self.plot_x, y=self.worker_thread.avg_y)
        self.p_up.setData(x=self.plot_x, y=self.worker_thread.up_y)
        self.p_down.setData(x=self.plot_x, y=self.worker_thread.down_y)
        self.prop_params.unlock()

        plot_y = poly(a, b, c, self.plot_x, i)
        self.p_proposal.setData(x=self.plot_x, y=plot_y)


    def run(self):
        # Start worker thread
        self.worker_thread.start()

        # Start main Qt GUI
        self.app.exec_()


if __name__ == '__main__':
    # np.random.seed(0)
    plotter = ProposalPlotter()
    plotter.run()

