#!/usr/bin/env python

import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np


class PlotWidgetWithKeyboard(pg.PlotWidget):
    def keyPressEvent(self, e):
        quit_keys = [QtCore.Qt.Key_Escape, QtCore.Qt.Key_Q]
        if e.key() in quit_keys:
            app = QtGui.QApplication.instance()
            app.exit()


class TomsPlotter():
    def __init__(self):
        self.app = QtGui.QApplication([])
        self.mw = QtGui.QMainWindow()
        self.mw.setWindowTitle("Tom's plotting test")
        self.mw.resize(400, 400)
        
        self.pw = PlotWidgetWithKeyboard()
        self.mw.setCentralWidget(self.pw)

        self.mw.show()
        # Bring window to foreground
        self.mw.raise_()

        self.p1 = self.pw.plot(pen=None, symbol='x')
        self.p1.getViewBox().disableAutoRange()
        self.p1.getViewBox().setRange(xRange=(-4,4), yRange=(-4,4))

        self.t = QtCore.QTimer()
        self.t.timeout.connect(self.update)
        self.t.start(50)


    def update(self):
        x = np.random.normal(size=1000)
        y = np.random.normal(size=1000)
        self.p1.setData(x=x, y=y, symbol='t')


    def run(self):
        self.app.exec_()


if __name__ == '__main__':
    plotter = TomsPlotter()
    plotter.run()
