#!/usr/bin/env python

import pandas as pd
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
from mcmcthread import MCMCThread
from updatinggui import UpdatingGUI
import os
import copy


# FIXME NAUGHTY GLOBALS!!
# FIXME NAUGHTY GLOBALS!!
# FIXME NAUGHTY GLOBALS!!

# How good a fit we kind of expect to be getting
penalty_stddev = 0.01

# The maximum value of background noise we're willing to consider
max_background = 0.5


# Where we're slicing between
freq_index_min = 105000
freq_index_max = 110000

# ... and use these to set our valid x ranges
prior_x0_min = freq_index_min 
prior_x0_max = freq_index_max 

# Prior for the width parameters (assume same for L and R)
prior_gamma_min = 0
prior_gamma_max = 1000

# Prior for the amplitude
prior_ampl_min = 0
prior_ampl_max = 3

# FIXME NAUGHTY GLOBALS!!
# FIXME NAUGHTY GLOBALS!!
# FIXME NAUGHTY GLOBALS!!


def get_temps(data):
    """Return list of tuples of column name and actual value of temperature."""
    temps = []
    for col in data.columns:
        if col == 'freq':
            continue
        temp = float(col[7:-1])
        temps.append((col, temp))
    return temps


def asym_lorentz(x, x0, gL, gR, amp):
    """
    Asymmetric Lorentzian function, specified with central peak location, widths
    on the left and right hand side, as well as the maximum height of the peak.
    Expect x to be numpy array.
    """
    maskR = (x >= x0)
    maskL = np.ones_like(x) - maskR
    g = maskR * gR + maskL * gL
    return amp * g**2 / ( (x - x0)**2 + g**2  )


class PeakParams:
    """Parameters to fit one asymmetric  Lorentzian peak."""
    
    def __init__(self):
        self.x0 = 0
        self.gammaL = 1
        self.gammaR = 1
        self.amp = 1

    def evaluate(self, x):
        """Evaluate the y-value of this peak at the given x-coordinate."""
        return asym_lorentz(x, 
                self.x0, 
                self.gammaL,
                self.gammaR,
                self.amp)

    def fwhm(self):
        """
        The full width at half maximum is just the sum of the two width
        parameters.
        """
        return self.gammaL + self.gammaR


class BackgroundParams:
    def __init__(self):
        self.mean = 0.1


class MultiplePeakParams:
    """Parmaters to fit multiple peaks in a linear data set."""
    
    def __init__(self, data_freq):
        self.data_freq = data_freq
        self.peaks = []
        self.background = BackgroundParams()

        # FIXME testing
        # FIXME testing
        peak = PeakParams()
        peak.x0 = 1000000
        peak.gammaL = 100
        peak.gammaR = 500
        peak.amp = 2
        peak2 = PeakParams()
        peak2.x0 = 1002000
        peak2.gammaL = 200
        peak2.gammaR = 500
        peak2.amp = 1.5
        self.peaks.append(peak)
        self.peaks.append(peak2)
        # FIXME testing
        # FIXME testing

    def make_copy(self):
        # FIXME there must be a more Pythonic way of doing this
        new = MultiplePeakParams(self.data_freq)
        new.background.mean = self.background.mean
        new.peaks = copy.deepcopy(self.peaks)

    def num_peaks(self):
        return len(self.peaks)

    def xy_coords(self):
        result = np.ones_like(self.data_freq) * self.background.mean
        for peak in self.peaks:
            result += peak.evaluate(self.data_freq)
        return self.data_freq, result


class PeakFittingMCMC(MCMCThread):
    def __init__(self, data_freq, data_ampl):
        self.data_freq = data_freq
        self.data_ampl = data_ampl
        init_params = MultiplePeakParams(data_freq)
        super().__init__(init_params)

    def propose_step(self):
        """
        Propose a metropolis-hastings step in the parameter space. Return both
        the new parameters and the logarithm of the qfactor ratio associated
        with the step.
        """
        proposal = self.curr_params.make_copy()

        # Can these probabilities change over time? I think so...
        options_nopeaks = {
                'ALTERBKG': 0.2,
                'ADD': 0.8,
                }

        options_somepeaks = {
                'ALTERBKG': 0.1,
                'REMOVE': 0.2,
                'ADD': 0.2,
                'ALTER': 0.5,
                }

        options_nopeaks_keys = list(options_nopeaks.keys())
        options_nopeaks_probs = [options_nopeaks[key] for key in options_nopeaks_keys]
        options_somepeaks_keys = list(options_somepeaks.keys())
        options_somepeaks_probs = [options_somepeaks[key] for key in options_somepeaks_keys]

        # FIXME!!
        return self.curr_params, 0

    def compute_log_prob(self, params):
        """
        Compute a quantity proportional to the target probability distribution,
        returning the logarithm of the density.
        """
        fit_freq,fit_ampl = params.xy_coords()
        log_likelihood = - np.sum(
                (fit_ampl - fit_freq)**2 / ( 2 * penalty_stddev**2 )
                )

        # Uniform prior over background amplitude
        log_prior = - np.log(max_background)

        # Prior for any given peak
        # TODO might not just want uniform priors here...
        log_prior += - params.num_peaks() * (  
                np.log(prior_x0_max - prior_x0_min) +
                2 * np.log(prior_gamma_max - prior_gamma_min) +
                np.log(prior_ampl_max - prior_ampl_min)
                )

        return log_likelihood + log_prior



class SarahDataGUI(UpdatingGUI):
    def __init__(self):
        self._load_data()
        super().__init__(
                title='RUS Test 1',
                width=700,
                height=400)
    
    def _load_data(self):
        base_path = '/Users/thomas/Dropbox/DATA FOR TOM/'
        filename = 'Ti64-001_LT_high.txt'
        # filename = 'Ti64-001_LT_low.txt'
        filepath = os.path.join(base_path, filename)
        data = pd.read_csv(filepath, delim_whitespace=True)
        temp_name = get_temps(data)[0][0]
        self.data_freq = data.loc[freq_index_min:freq_index_max, 'freq'].values
        self.data_ampl = data.loc[freq_index_min:freq_index_max, temp_name].values

    def populate_gui(self):
        """Create plots and other things in the GUI"""
        self.plot1 = self.win.addPlot()
        # self.plot1.disableAutoRange()
        # self.plot1.setRange(xRange=(0,1), yRange=(0,1))

        # self.x_slice_indicators = []
        # for x in self.x_slices:
        #     line = pg.InfiniteLine(x, 
        #             pen=pg.mkPen(color=(20,20,100), style=QtCore.Qt.DashLine, alpha=0.3))
        #     self.x_slice_indicators.append(line)
        #     self.plot1.addItem(line)

        self.data_plot = self.plot1.plot(
                x=self.data_freq, y=self.data_ampl,
                pen=None, symbol='o', symbolBrush='c', symbolPen='c', symbolSize=1)

        # Disable auto range once the axes are fixed from the data plot
        self.plot1.disableAutoRange()

        self.fitted_plot = self.plot1.plot(pen='r', symbol=None)

        # self.line_plot = self.plot1.plot(
        #         pen='y', symbol='o', symbolBrush='y')

        # self.label_eff = pg.TextItem('eff = ')
        # self.plot1.addItem(self.label_eff)
        # self.label_eff.setPos(0.0, 1.0)

        # self.label_debug = pg.TextItem('debug')
        # self.plot1.addItem(self.label_debug)
        # self.label_debug.setPos(0.0, 0.92)

    def create_worker(self):
        """Return an instance of the worker thread object"""
        return PeakFittingMCMC(self.data_freq, self.data_ampl)

    def update(self):
        """Refresh data in any plots"""
        self.worker.lock()
        fit_freq, fit_ampl = self.worker.curr_params.xy_coords()
        self.worker.unlock()
        self.fitted_plot.setData(x=fit_freq, y=fit_ampl)


if __name__ == '__main__':
    np.random.seed(0)
    gui = SarahDataGUI()
    gui.run()

