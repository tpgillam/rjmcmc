import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore


class GraphicsWindowWithKeyBindings(pg.GraphicsWindow):
    """
    Like a graphics window, but will quit when pressing 'q' or ESC.
    """

    def __init__(self, worker=None):
        self.worker = worker
        pg.GraphicsWindow.__init__(self)

    def keyPressEvent(self, e):
        quit_keys = [QtCore.Qt.Key_Escape, QtCore.Qt.Key_Q]
        if e.key() in quit_keys:
            app = QtGui.QApplication.instance()
            app.exit()
        elif e.key() == QtCore.Qt.Key_P and self.worker is not None:
            if not self.worker.quitting:
                self.worker.quitting = True
            else:
                self.worker.quitting = False
                self.worker.start()


class UpdatingGUI:
    """
    Basic class to represent a simple pyqtgraph application, with a main window
    ready to be loaded with plots. Assumes there will be a worker thread doing
    the heavy lifting, with the GUI periodically updating to represent what is
    happening.
    """

    def __init__(self, title='Updating GUI', width=400, height=400,
            update_time_ms=20):
        self._title = title
        self._width = width
        self._height = height
        self._update_time_ms = update_time_ms
        # TODO accept arguments?
        self.worker = self.create_worker()
        self._create_window()
        self.populate_gui()

    def _create_window(self):
        self.app = QtGui.QApplication([])
        self.win = GraphicsWindowWithKeyBindings(self.worker)
        self.win.setWindowTitle(self._title)
        self.win.resize(self._width, self._height)
        self.win.show()
        # This is needed to bring the window to the foreground
        self.win.raise_()

        # Initialise GUI update loop
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(self._update_time_ms)

    def populate_gui(self):
        """Create plots and other things in the GUI"""
        raise NotImplementedError()

    def create_worker(self):
        """
        Return an instance of the worker thread object, or None if no worker is
        required.
        """
        return None

    def update(self):
        """Refresh data in any plots"""
        raise NotImplementedError()

    def run(self):
        if self.worker is not None:
            self.worker.start()
        self.app.exec_()


